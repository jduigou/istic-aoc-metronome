package engine;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import command.Command;

public class MetronomeEngineImplTest {

	@Mock
	private Schedule timer;
	
	@Mock
	private Command command1;
	
	@Mock
	private Command command2;
	
	private MetronomeEngine me;

	public class ScheduleTest implements Schedule {
		
		private int nbInvocations;
		
		public ScheduleTest(int nbInvocations) {
			this.nbInvocations = nbInvocations;
		}
		
		@Override
		public void runPeriodically(Command _cmd, int _period) {
			
			for(int i= 0; i<nbInvocations; i++) {
				_cmd.execute();
			}
			
		}
		
		@Override
		public void runLater(Command _cmd, int _delay) {

		}
		
		@Override
		public void cancel(Command _cmd) {

		}
	}
	
	@Before
	public void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
		me = new MetronomeEngineImpl();
		me.setTimer(timer);
		me.setTempo(120);
		me.setBeatsPerBar(4);
	}

	@Test
	public void setRunningTest1() 
	{
		me.setRunning(true);
		Mockito.verify(timer, Mockito.times(1)).runPeriodically(Matchers.any(Command.class), Matchers.anyInt());
	}
	
	@Test
	public void setRunningTest2() 
	{
		me.setTempo(0);
		me.setRunning(true);
		Mockito.verify(timer, Mockito.times(0)).runPeriodically(Matchers.any(Command.class), Matchers.anyInt());
	}
	
	@Test
	public void testTempoChangedCommand() {
		
		me.setTempoChangedCommand(command1);
		me.setTempo(19);
		Mockito.verify(command1, Mockito.times(1)).execute();
		assertEquals(19, me.getTempo().intValue());
		
	}
	
	@Test
	public void testBeatPerBarChangedCommand() {
		
		me.setBeatsPerBarChangedCommand(command1);
		me.setBeatsPerBar(2);
		Mockito.verify(command1, Mockito.times(1)).execute();
		assertEquals(2, me.getBeatsPerBar().intValue());
		
	}
	
	@Test
	public void testBeatCommand() {

		me.setTimer(new ScheduleTest(1));
		
		me.setBeatCommand(command1);
		me.setRunning(true);
		
		Mockito.verify(command1, Mockito.times(1)).execute();
		
	}
	
	@Test
	public void testBarCommand() {

		me.setTimer(new ScheduleTest(4));
		
		me.setBeatCommand(command1);
		me.setBarCommand(command2);
		me.setRunning(true);
		
		Mockito.verify(command1, Mockito.times(4)).execute();
		Mockito.verify(command2, Mockito.times(1)).execute();
		
	}

}
