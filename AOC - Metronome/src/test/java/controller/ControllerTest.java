package controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import command.Command;
import view.ButtonME;
import view.Slider;
import view.ViewController;
import engine.MetronomeEngine;

public class ControllerTest {
	
	@Mock
	private MetronomeEngine me;
	
	@Mock
	private ViewController view;
	
	private Controller controller;
	
	private SimulatedButton buttonStart = new SimulatedButton();
	private SimulatedButton buttonStop = new SimulatedButton();
	private SimulatedButton buttonInc = new SimulatedButton();
	private SimulatedButton buttonDec = new SimulatedButton();
	private SimulatedSlider slider = new SimulatedSlider();
	
	public class SimulatedButton implements ButtonME {

		private Command cmd;
		
		public void fireEvent() {
			cmd.execute();
		}
		
		@Override
		public void setClickCommand(Command _command) {
			cmd = _command;
		}
		
	}
	
	public class SimulatedSlider implements Slider {

		private OnSliderChangedCommand cmd;
		
		public void fireEvent(Number nb) {
			cmd.execute(nb);
		}
		
		@Override
		public void setSliderChangedCommand(OnSliderChangedCommand cmd) {
			this.cmd = cmd;
		}
		
	}
	
	@Before
	public void setUp() {
		
		MockitoAnnotations.initMocks(this);
		controller = new Controller(me);
		
		Mockito.when(view.getButtonStart()).thenReturn(buttonStart);
		Mockito.when(view.getButtonStop()).thenReturn(buttonStop);
		Mockito.when(view.getButtonInc()).thenReturn(buttonInc);
		Mockito.when(view.getButtonDec()).thenReturn(buttonDec);
		Mockito.when(view.getSlider()).thenReturn(slider);
		controller.setView(view);
		
	}
	
	@Test
	public void testStart() {
		
		buttonStart.fireEvent();
		Mockito.verify(me).setRunning(true);
		
	}
	
	@Test
	public void testStop() {
		
		buttonStop.fireEvent();
		Mockito.verify(me).setRunning(false);
		
	}
	
	@Test
	public void testBeatsPerBarInc() {
		
		Mockito.when(me.getBeatsPerBar()).thenReturn(4);
		buttonInc.fireEvent();
		Mockito.verify(me).setBeatsPerBar(5);
		
	}
	
	@Test
	public void testBeatsPerBarIncLimit() {
		
		Mockito.when(me.getBeatsPerBar()).thenReturn(7);
		buttonInc.fireEvent();
		Mockito.verify(me, Mockito.times(0)).setBeatsPerBar(Matchers.anyInt());
		
	}
	
	@Test
	public void testBeatsPerBarDec() {
		
		Mockito.when(me.getBeatsPerBar()).thenReturn(4);
		buttonDec.fireEvent();
		Mockito.verify(me).setBeatsPerBar(3);
		
	}
	
	@Test
	public void testBeatsPerBarDecLimit() {
		
		Mockito.when(me.getBeatsPerBar()).thenReturn(2);
		buttonDec.fireEvent();
		Mockito.verify(me, Mockito.times(0)).setBeatsPerBar(Matchers.anyInt());
		
	}
	
	@Test
	public void testSlider() {
		
		Number nb = 50;
		slider.fireEvent(nb);
		Mockito.verify(me).setTempo(220*nb.intValue()/100);
	}
	
}
