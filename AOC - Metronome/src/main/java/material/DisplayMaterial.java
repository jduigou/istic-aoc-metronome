package material;

public interface DisplayMaterial {

	public void switchOn(int i);
	
	public void switchOff(int i);
	
	public void showTempo(int v);
	
	public void showBeatsPerBar(int v);
	
}
