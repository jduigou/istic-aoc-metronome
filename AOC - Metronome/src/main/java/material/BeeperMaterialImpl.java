package material;

import java.net.URISyntaxException;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class BeeperMaterialImpl implements BeeperMaterial {

	private String soundURL = "/javaFX/sound.mp3";
	private Media sound;
	private MediaPlayer mediaPlayer;
	
	public BeeperMaterialImpl() {
		try {
			sound = new Media(getClass().getResource(soundURL).toURI().toString());
			mediaPlayer = new MediaPlayer(sound);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void emitBeep() {
		mediaPlayer.stop();
		mediaPlayer.play();
	}

}
