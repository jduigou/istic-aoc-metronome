package material;

import engine.Schedule;

public interface ViewMaterial {

	public SliderMaterial getSlider();
	
	public KeyboardMaterial getKeyboard();
	
	public DisplayMaterial getDisplay();
	
	public Schedule getSchedule();
	
	public BeeperMaterialImpl getBeeper();
	
}
