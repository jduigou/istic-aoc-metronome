package material;

import java.util.Map;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

import com.google.common.collect.Maps;

public class KeyboardMaterialImpl implements KeyboardMaterial {

	private Map<Integer, Boolean> values;
	private Map<Integer, Button> buttons;
	
	public KeyboardMaterialImpl(Map<Integer, Button> buttons) {
		
		values = Maps.newHashMap();
		this.buttons = buttons;
		
		for(Integer i : buttons.keySet()) {
			Button btn = buttons.get(i);
			btn.setOnMousePressed(new EventHandler<Event>() {
				@Override
				public void handle(Event event) {
					values.put(i, true);
				}
			});
			btn.setOnMouseReleased(new EventHandler<Event>() {
				@Override
				public void handle(Event event) {
					values.put(i, false);
				}
			});
		}
		
	}
	
	@Override
	public boolean isKeyPressed(int i) {
		if (values.containsKey(i)) {
			return values.get(i);
		}
		return false;
	}

}
