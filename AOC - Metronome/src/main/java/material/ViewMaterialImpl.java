package material;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

import com.google.common.collect.Maps;

import engine.Schedule;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.shape.Sphere;

public class ViewMaterialImpl implements Initializable, ViewMaterial {

	@FXML private Sphere sphere1;
	@FXML private Sphere sphere2;
	
	@FXML private Button btnInc;
	@FXML private Button btnDec;
	@FXML private Button btnStart;
	@FXML private Button btnStop;
	
	@FXML private Label displayBeat;
	@FXML private Label displayBar;
	
	@FXML private Slider sliderFX;
	
	private SliderMaterial slider;
	private KeyboardMaterial keyboard;
	private DisplayMaterial display;
	private Schedule timer;
	private BeeperMaterialImpl beeper;
	
	public void init(Schedule timer) {
		
		this.timer = timer;
		
		Map<Integer, Sphere> spheres = Maps.newHashMap();
		spheres.put(1, sphere1);
		spheres.put(2, sphere2);
		display = new DisplayMaterialImpl(displayBeat, displayBar, spheres);
		
		Map<Integer, Button> buttons = Maps.newHashMap();
		buttons.put(1, btnStart);
		buttons.put(2, btnStop);
		buttons.put(3, btnInc);
		buttons.put(4, btnDec);
		keyboard = new KeyboardMaterialImpl(buttons);
		
		slider = new SliderMaterialImpl(sliderFX);
		beeper = new BeeperMaterialImpl();
		
	}
	
	@Override
	public SliderMaterial getSlider() {
		return slider;
	}

	@Override
	public KeyboardMaterial getKeyboard() {
		return keyboard;
	}

	@Override
	public DisplayMaterial getDisplay() {
		return display;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Schedule getSchedule() {
		return timer;
	}

	@Override
	public BeeperMaterialImpl getBeeper() {
		return beeper;
	}


}
