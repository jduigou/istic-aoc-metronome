package material;

import java.util.Map;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;

public class DisplayMaterialImpl implements DisplayMaterial {

	private Map<Integer, Sphere> spheres;
	private Label displayTempo;
	private Label displayBeatsPerBar;
	
	public DisplayMaterialImpl(Label displayTempo, Label displayBeatsPerBar, Map<Integer, Sphere> spheres) {
		
		this.displayTempo = displayTempo;
		this.displayBeatsPerBar = displayBeatsPerBar;
		this.spheres = spheres;
		
	}
	
	@Override
	public void switchOn(int i) {
		if (spheres.containsKey(i)) {
			PhongMaterial material = new PhongMaterial();
	    	//Diffuse Color
	    	material.setDiffuseColor(Color.ORANGE);
	    	//Specular Color
	    	material.setSpecularColor(Color.ORANGE);
	    	spheres.get(i).setMaterial(material);
		}
	}

	@Override
	public void switchOff(int i) {
		if (spheres.containsKey(i)) {
			//Creating PhongMaterial
	    	PhongMaterial material = new PhongMaterial();
	    	//Diffuse Color
	    	material.setDiffuseColor(Color.GRAY);
	    	//Specular Color
	    	material.setSpecularColor(Color.GRAY);
	    	spheres.get(i).setMaterial(material);
		}
	}

	@Override
	public void showTempo(int v) {
		displayTempo.setText(Integer.toString(v));
	}

	@Override
	public void showBeatsPerBar(int v) {
		displayBeatsPerBar.setText(Integer.toString(v));
	}
	
}
