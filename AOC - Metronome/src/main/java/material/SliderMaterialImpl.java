package material;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Slider;

public class SliderMaterialImpl implements SliderMaterial {

	private Double position=0.00;
	private Slider slider;
	
	public SliderMaterialImpl(Slider slider) {
		this.slider = slider;
		slider.setOnMouseDragged(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				position = slider.getValue();
			}
		});
	}
	
	@Override
	public Double getPosition() {
		return position;
	}

}
