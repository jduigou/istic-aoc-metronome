package controller;

import java.net.URL;
import java.util.ResourceBundle;

import engine.Schedule;
import javaFX.ButtonFX;
import javaFX.FXBeeper;
import javaFX.FXDisplay;
import javaFX.FXLED;
import javaFX.SliderFX;
import view.Beeper;
import view.ButtonME;
import view.Display;
import view.LED;
import view.ViewController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.shape.Sphere;

public class ControllerFX implements Initializable, ViewController{
	
	@FXML private Sphere sphere1;
	@FXML private Sphere sphere2;
	
	@FXML private Button btnInc;
	@FXML private Button btnDec;
	@FXML private Button btnStart;
	@FXML private Button btnStop;
	
	@FXML private Label displayBeat;
	@FXML private Label displayBar;
	
	@FXML private Slider sliderFX;
	
	private LED led1;
	private LED led2;
	private ButtonME buttonInc;
	private ButtonME buttonDec;
	private ButtonME buttonStart;
	private ButtonME buttonStop;
	private Beeper beeper;
	private Display display;
	private view.Slider slider;
	
	private Schedule timer;


	@Override
    public void initialize(URL location, ResourceBundle resources)
    {

    }
	
	public void init() {
		
		led1 = new FXLED(sphere1, timer);
		led2 = new FXLED(sphere2, timer);
    	
    	beeper = new FXBeeper();
    	
    	buttonInc = new ButtonFX(btnInc);
    	buttonDec = new ButtonFX(btnDec);
    	buttonStart = new ButtonFX(btnStart);
    	buttonStop = new ButtonFX(btnStop);
    	
    	display = new FXDisplay(displayBeat, displayBar);
    	
    	slider = new SliderFX(sliderFX);
	}
	
	@Override
    public Schedule getSchedule() {	
		return timer;
    }

    @Override
	public void setSchedule(Schedule timer) {
		this.timer = timer;
	}
	
	@Override
	public LED getLed1() {
		return led1;
	}

	@Override
	public void setLed1(LED led1) {
		this.led1 = led1;
	}

	@Override
	public LED getLed2() {
		return led2;
	}

	@Override
	public void setLed2(LED led2) {
		this.led2 = led2;
	}

	@Override
	public ButtonME getButtonInc() {
		return buttonInc;
	}

	@Override
	public void setButtonInc(ButtonME buttonInc) {
		this.buttonInc = buttonInc;
	}

	@Override
	public ButtonME getButtonDec() {
		return buttonDec;
	}

	@Override
	public void setButtonDec(ButtonME buttonDec) {
		this.buttonDec = buttonDec;
	}

	@Override
	public ButtonME getButtonStart() {
		return buttonStart;
	}

	@Override
	public void setButtonStart(ButtonME buttonStart) {
		this.buttonStart = buttonStart;
	}

	@Override
	public ButtonME getButtonStop() {
		return buttonStop;
	}

	@Override
	public void setButtonStop(ButtonME buttonStop) {
		this.buttonStop = buttonStop;
	}

	@Override
	public Beeper getBeeper() {
		return beeper;
	}

	@Override
	public void setBeeper(Beeper beeper) {
		this.beeper = beeper;
	}

	@Override
	public Display getDisplay() {
		return display;
	}

	@Override
	public void setDisplay(Display display) {
		this.display = display;
	}

	@Override
	public view.Slider getSlider() {
		return slider;
	}

	@Override
	public void setSlider(view.Slider slider) {
		this.slider = slider;
	}
}
