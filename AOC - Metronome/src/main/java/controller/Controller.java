package controller;

import engine.MetronomeEngine;
import engine.Schedule;
import view.Slider;
import view.ViewController;
import command.Command;
/**
 * Created by david on 06/10/2014.
 */
public class Controller
{
	private static int MAX_TEMPO = 220;
	
	
	private ViewController view;
	private MetronomeEngine me;
    
    private static final int DEFAULT_TEMPO = 120;
    private static final int DEFAULT_BEATSPERBAR = 4;
    
    public Controller(MetronomeEngine me)
    {
    	this.me = me;
    	init();
    }
    
    private void init() {
    	
    	me.setBeatCommand(new Command() {
			@Override
			public void execute() {
				handleBeatEvent();
			}
    	});
    	
    	me.setBarCommand(new Command() {
			@Override
			public void execute() {
				handleBarEvent();
			}
    	});
    	
    	me.setTempoChangedCommand(new Command() {
			@Override
			public void execute() {
				handleTempoChanged();
			}  		
    	});
    	
    	me.setBeatsPerBarChangedCommand(new Command() {
			@Override
			public void execute() {
				handleBeatsPerBarChanged();
			}
		});
    	
//    	if (me.getTempo() == null) {
//    		me.setTempo(DEFAULT_TEMPO);
//    	}
    	
    	
    	
    }
    
    /**
     * init the view
     * set commands to the UI objects
     */
    private void initView() {
    	
		view.getButtonStop().setClickCommand(new Command() {
			@Override
			public void execute() {
				stop();
			}
		});	
		
		view.getButtonStart().setClickCommand(new Command() {
			@Override
			public void execute() {
				start();
			}
		});
		
		view.getButtonInc().setClickCommand(new Command() {
			@Override
			public void execute() {
				Integer v = me.getBeatsPerBar();
				if (v == null) v = 1;
				if (v < 7) me.setBeatsPerBar(v+1);
			}
		});
		
		view.getButtonDec().setClickCommand(new Command() {
			@Override
			public void execute() {
				Integer v = me.getBeatsPerBar();
				if (v == null) v = 3;
				if (v > 2) me.setBeatsPerBar(v-1);
			}
		});
		
		view.getSlider().setSliderChangedCommand(new Slider.OnSliderChangedCommand() {
			
			@Override
			public void execute(Number nb) {
				int tempo = MAX_TEMPO*nb.intValue()/100;
				System.out.println("Slider changed :"+tempo);
				me.setTempo(tempo);
			}
		});
		
    }

    /**
     * Start the metronome
     */
    public void start()
    {    	
    	if (me.getBeatsPerBar() == null) {
    		me.setBeatsPerBar(DEFAULT_BEATSPERBAR);
    	}
    	
        me.setRunning(true);
    }
    
    /**
     * Function called when the start event is received
     */
    public void handleStart()
    {
    	System.out.println("metronome started");
    }

    /**
     * Stop the metronome
     */
    public void stop()
    {
        me.setRunning(false);
    }

    /**
     * Update the View when BeatEvent is received
     */
    public void handleBeatEvent()
    {
    	view.getLed1().flash();
    	view.getBeeper().beep();
    }
    
    /**
     * Update the View when BarEvent is received
     */
    public void handleBarEvent()
    {
    	view.getLed2().flash();
    }
    
    /**
     * Update the View when the tempo has been changed
     */
    public void handleTempoChanged() {
    	
    	view.getDisplay().displayTempo(me.getTempo());
    	
    }
    
    /**
     * Update the view when the beats per bar has been changed
     */
    public void handleBeatsPerBarChanged() {
    	
    	view.getDisplay().displayBeatsPerBar(me.getBeatsPerBar());
    	
    }
	
    /**
     * Get the timer used by the Metronome Engine
     * to execute commands periodically
     * @return the timer
     */
	public Schedule getTimer()
	{
		return me.getTimer();
	}

	/**
	 * Get the view that contains 
	 * the UI objects
	 * @return view
	 */
	public ViewController getView() {
		return view;
	}

	/**
	 * Associate a view to the Metronome Engine Controller
	 */
	public void setView(ViewController view) {
		
		this.view = view;
		initView();
		
	}
	
}
