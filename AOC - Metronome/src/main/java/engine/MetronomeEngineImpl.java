package engine;

import java.util.Date;

import command.Command;

/**
 * Created by david on 06/10/2014.
 */
public class MetronomeEngineImpl implements MetronomeEngine
{

    private Command barCommand;
    private Command beatCommand;
    private Command tempoChangedCommand;
    private Command beatsPerBarChangedCommand;
    
    private Integer tempo=0;
    private Integer beatsPerBar;
    private boolean running;
    private Schedule timer;
    private int count;
    private Command cmde;
    
    @Override
    public void setBarCommand(Command _cmd)
    {
        barCommand = _cmd;
    }

    @Override
    public void setBeatCommand(Command _cmd)
    {
        beatCommand = _cmd;
    }

    @Override
    public void setTempoChangedCommand(Command _cmd) {
        tempoChangedCommand = _cmd;
    }

    @Override
    public Integer getTempo()
    {
        return tempo;
    }

    @Override
    public void setTempo(Integer _tempo)
    {
        tempo = _tempo;
        if (running) {
        	initTickCommand();
        }
        if (tempoChangedCommand != null) {
        	tempoChangedCommand.execute();
        }
    }

    @Override
    public Integer getBeatsPerBar()
    {
        return beatsPerBar;
    }

    @Override
    public void setBeatsPerBar(Integer _beatsPerBar)
    {
        beatsPerBar = _beatsPerBar;
        if (beatsPerBarChangedCommand != null) {
        	beatsPerBarChangedCommand.execute();
        }
    }

    @Override
    public boolean isRunning()
    {
        return running;
    }

    @Override
    public void setRunning(boolean _isRunning)
    {
    	if (running == _isRunning) {
    		return;
    	}
    	
    	running = _isRunning;
        
        if(running && tempo >0)
        {
        	count = 0;

            initTickCommand();
			
			System.out.println("me started");
			
        }
        if(!running)
        {
        	timer.cancel(cmde);
        	System.out.println("me stopped");
        }
    }
    
    private void initTickCommand() {
    	
    	if (cmde != null) {
    		timer.cancel(cmde);
    	}
    	
    	int period =(int)(60.00/tempo*1000);
        
        cmde = new Command() {
			@Override
			public void execute() {
				tick();	
			}
		};
  	
		timer.runPeriodically(cmde, period);
    }

    @Override
    public void setTimer(Schedule _timer) 
    {
        timer = _timer;
    }
   
    /*
     * Execute BeatCmd & BarCmd
     */
    public void tick()
    {
    	count++;
    	System.out.println("Tempo: "+ (new Date()).getTime()/1000);
    	beatCommand.execute();
    	if((count%beatsPerBar) == 0)
    	{
        	barCommand.execute();
    	}
    }
    
    @Override
    public Schedule getTimer() {
    	return timer;
    }

    @Override
	public void setBeatsPerBarChangedCommand(Command beatsPerBarChangedCommand) {
		this.beatsPerBarChangedCommand = beatsPerBarChangedCommand;
	}
}
