package engine;

import command.Command;

/**
 * Created by david on 06/10/2014.
 */
public interface MetronomeEngine
{

    /**
     * Set the Bar Command
     * @param _cmd
     */
    public void setBarCommand(Command _cmd);

    /**
     * Set the Beat Command
     * @param _cmd
     */
    public void setBeatCommand(Command _cmd);

    /**
     * Set the command to invoke when the tempo changed
     * @param _cmd
     */
    public void setTempoChangedCommand(Command _cmd);
    
    /**
     * Set the command to invoke when the beats per bar
     * has been changed
     * @param beatsPerBarChangedCommand
     */
	public void setBeatsPerBarChangedCommand(Command beatsPerBarChangedCommand);
	
    /**
     * Get the tempo
     * @return the current tempo
     */
    public Integer getTempo();

    /**
     * Set current tempo
     * @param _tempo current tempo
     */
    public void setTempo(Integer _tempo);

    /**
     * Get the number of beats per bar
     * @return the number of beats per bar
     */
    public Integer getBeatsPerBar();

    /**
     * Set the number of beats per bar
     * @param _beatsPerBar number of beats per bar
     */
    public void setBeatsPerBar(Integer _beatsPerBar);

    /**
     * get current state of the metronome
     * @return true if the metronome is running
     */
    public boolean isRunning();

    /**
     * set current state of the metronome
     * @param _isRunning true if the metronome is currently running
     */
    public void setRunning(boolean _isRunning);

    /**
     * set the timer that will be called by the engine
     * to handle multi-threading
     * @param _timer
     */
    public void setTimer(Schedule _timer);
    
    /**
     * get the timer used by the engine to run the commands
     * @return
     */
    public Schedule getTimer();

}
