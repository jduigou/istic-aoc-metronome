package engine;

import command.Command;

/**
 * Created by david on 15/10/2014.
 */
public interface Schedule {


    /**
     * execute a cmd periodically 
     * @param _cmd
     * @param _period : nb of milliseconds in the period
     */
    public void runPeriodically(Command _cmd, int _period);

    /**
     * execute a cmd after the delay
     * @param _cmd
     * @param _delay : nb of milliseconds
     */
    public void runLater(Command _cmd, int _delay);

    public void cancel(Command _cmd);
   

}
