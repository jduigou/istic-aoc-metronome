package adapter;

import material.KeyboardMaterial;
import command.Command;
import view.ButtonME;

public class ButtonAdapter implements ButtonME {
	
	private Command cmd;
	private int keyID;
	private KeyboardMaterial keyboard;
	
	private boolean executionNeeded = false;

	
	public ButtonAdapter(KeyboardMaterial keyboard, Integer i) {
		
		this.keyID = i;
		this.keyboard = keyboard;
		
	}
	
	@Override
	public void setClickCommand(Command _command) {
		
		this.cmd = _command;
		
	}
	
	public void read() {

		if (keyboard.isKeyPressed(keyID)) {
			System.out.println("key pressed!");
			executionNeeded = true;
		}
		
		if (executionNeeded && !keyboard.isKeyPressed(keyID) && cmd != null) {
			
			cmd.execute();
			executionNeeded = false;
			
		}
		
	}

}
