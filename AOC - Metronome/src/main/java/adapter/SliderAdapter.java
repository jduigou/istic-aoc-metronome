package adapter;

import material.SliderMaterial;
import view.Slider;

public class SliderAdapter implements Slider {

	private SliderMaterial materialSlider;
	private OnSliderChangedCommand cmd;

	private int cpt = 0;
	private Double lastPosition;
	
	public SliderAdapter(SliderMaterial sliderMaterial) {
		
		this.materialSlider = sliderMaterial;
		lastPosition = sliderMaterial.getPosition();
		
	}
	
	public void read() {
		
		Double p = materialSlider.getPosition();
		//System.out.println("Slider position: "+p);
		if (p != lastPosition) {
			lastPosition = p;
			cpt = 0;
		}else{
			cpt++;
			if (cpt == 5 && cmd != null) {
				cmd.execute(p);
			}
		}
		
	}
	
	
	@Override
	public void setSliderChangedCommand(OnSliderChangedCommand cmd) {
		
		this.cmd = cmd;
		
	}

}
