package adapter;

import command.Command;
import material.KeyboardMaterial;
import material.ViewMaterial;
import engine.Schedule;
import view.Beeper;
import view.ButtonME;
import view.Display;
import view.LED;
import view.Slider;
import view.ViewController;

public class ViewAdapter implements ViewController {

	private ViewMaterial viewMaterial;
	
	private Schedule timer;
	private Slider sliderAdapter;
	private ButtonME buttonStartAdapter;
	private ButtonME buttonStopAdapter;
	private ButtonME buttonIncAdapter;
	private ButtonME buttonDecAdapter;
	private LED led1;
	private LED led2;
	private Display display;
	private Beeper beeper;
	
	public ViewAdapter(ViewMaterial materialView) {
		this.viewMaterial = materialView;
		init();
	}
	
	private void init() {
		
		timer = viewMaterial.getSchedule();
		KeyboardMaterial keyboard = viewMaterial.getKeyboard();
		sliderAdapter = new SliderAdapter(viewMaterial.getSlider());
		buttonStartAdapter = new ButtonAdapter(keyboard, 1);
		buttonStopAdapter = new ButtonAdapter(keyboard, 2);
		buttonIncAdapter = new ButtonAdapter(keyboard, 3);
		buttonDecAdapter = new ButtonAdapter(keyboard, 4);
		led1 = new LEDAdapter(viewMaterial.getDisplay(), 1, timer);
		led2 = new LEDAdapter(viewMaterial.getDisplay(), 2, timer);
		display = new DisplayAdapter(viewMaterial.getDisplay());
		beeper = new BeeperAdapter(viewMaterial.getBeeper());
		
		timer.runPeriodically(new Command() {
			@Override
			public void execute() {
				((ButtonAdapter) buttonStartAdapter).read();
				((ButtonAdapter) buttonStopAdapter).read();
				((ButtonAdapter) buttonIncAdapter).read();
				((ButtonAdapter) buttonDecAdapter).read();
				((SliderAdapter) sliderAdapter).read();
			}
		}, 10);
		
	}
	
	@Override
	public LED getLed1() {
		return led1;
	}

	@Override
	public void setLed1(LED led1) {
		this.led1 = led1;
	}

	@Override
	public LED getLed2() {
		return led2;
	}

	@Override
	public void setLed2(LED led2) {
		this.led2 = led2;
	}

	@Override
	public ButtonME getButtonInc() {
		return buttonIncAdapter;
	}

	@Override
	public void setButtonInc(ButtonME btnInc) {
		buttonIncAdapter = btnInc;
	}

	@Override
	public ButtonME getButtonDec() {
		return buttonDecAdapter;
	}

	@Override
	public void setButtonDec(ButtonME btnDec) {
		buttonDecAdapter = btnDec;
	}

	@Override
	public ButtonME getButtonStart() {
		return buttonStartAdapter;
	}

	@Override
	public void setButtonStart(ButtonME btnStart) {
		buttonStartAdapter = btnStart;
	}

	@Override
	public ButtonME getButtonStop() {
		return buttonStopAdapter;
	}

	@Override
	public void setButtonStop(ButtonME btnStop) {
		buttonStopAdapter = btnStop;
	}

	@Override
	public Beeper getBeeper() {
		return beeper;
	}

	@Override
	public void setBeeper(Beeper beeper) {
		this.beeper = beeper;
	}

	@Override
	public Display getDisplay() {
		return display;
	}

	@Override
	public void setDisplay(Display display) {
		this.display = display;
	}

	@Override
	public Slider getSlider() {
		return sliderAdapter;
	}

	@Override
	public void setSlider(Slider slider) {
		sliderAdapter = slider;
	}

	@Override
	public void setSchedule(Schedule timer) {
		
	}

	@Override
	public Schedule getSchedule() {
		return viewMaterial.getSchedule();
	}

	
}
