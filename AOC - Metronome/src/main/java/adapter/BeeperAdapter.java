package adapter;

import material.BeeperMaterial;
import view.Beeper;

public class BeeperAdapter implements Beeper {

	public BeeperMaterial beeper;
	
	public BeeperAdapter(BeeperMaterial beeper) {
		this.beeper = beeper;
	}
	
	@Override
	public void beep() {
		beeper.emitBeep();
	}

}
