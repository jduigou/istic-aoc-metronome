package adapter;

import command.Command;
import engine.Schedule;
import material.DisplayMaterial;
import view.LED;

public class LEDAdapter implements LED {
	
	private int ledID;
	private DisplayMaterial display;
	private Schedule timer;
	
	public LEDAdapter(DisplayMaterial display, int i, Schedule timer) {
		
		this.timer = timer;
		this.display = display;
		ledID = i;
	}
	
	@Override
	public void flash() {

		display.switchOn(ledID);
		
		timer.runLater(new Command() {
			@Override
			public void execute() {
				display.switchOff(ledID);
			}
		}, 100);
		
	}

}
