package adapter;

import material.DisplayMaterial;
import view.Display;

public class DisplayAdapter implements Display {

	private DisplayMaterial displayMaterial;
	
	public DisplayAdapter(DisplayMaterial displayMaterial) {
		this.displayMaterial = displayMaterial;
	}
	
	@Override
	public void displayTempo(int _tempo) {
		displayMaterial.showTempo(_tempo);
	}

	@Override
	public void displayBeatsPerBar(int _beatsPerBar) {
		displayMaterial.showBeatsPerBar(_beatsPerBar);
	}

	
}
