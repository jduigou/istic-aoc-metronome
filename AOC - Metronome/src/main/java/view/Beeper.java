package view;

/**
 * Created by Johanna on 13/10/2014.
 */
public interface Beeper
{
    public void beep();
}
