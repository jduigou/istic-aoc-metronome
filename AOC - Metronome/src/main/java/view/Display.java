package view;

/**
 * Created by Johanna on 13/10/2014.
 */
public interface Display {

    /**
     * Set the text of the display
     * @param _tempo
     */
    public void displayTempo(int _tempo);
    
    /**
     * Set the text of the display
     * @param _beatsPerBar
     */
    public void displayBeatsPerBar(int _beatsPerBar);

}
