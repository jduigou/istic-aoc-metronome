package view;


public interface Slider {
	
    public void setSliderChangedCommand(OnSliderChangedCommand cmd);
    
	public interface OnSliderChangedCommand{
		public void execute(Number nb);
	}
}
