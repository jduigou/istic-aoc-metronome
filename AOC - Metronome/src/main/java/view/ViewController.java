package view;

import engine.Schedule;

public interface ViewController {

    /**
     * Returns Led1
     * @return led1
     */
	public LED getLed1();

	/**
	 * Change Led1
	 * @param led1
	 */
	public void setLed1(LED led1);
	
	/**
	 * Returns Led2
	 * @return led2
	 */
	public LED getLed2();

	/**
	 * Change Led2
	 * @param led2
	 */
	public void setLed2(LED led2);

	/**
	 * get the button for incrementing
	 * the beats per bar value
	 * @return ButtonME
	 */
	public ButtonME getButtonInc();

	/**
	 * set the button for incrementing
	 * the beats per bar value
	 * @param ButtonME
	 */
	public void setButtonInc(ButtonME btnInc);

	/**
	 * get the button for decrease
	 * the beats per bar value
	 * @return ButtonME
	 */
	public ButtonME getButtonDec();

	/**
	 * set the button for decreasing
	 * the beats per bar value
	 * @param ButtonME
	 */
	public void setButtonDec(ButtonME btnDec);

	/**
	 * get the start button
	 * @return ButtonME
	 */
	public ButtonME getButtonStart();

	/**
	 * set the start button
	 * @param btnStart
	 */
	public void setButtonStart(ButtonME btnStart);
	
	/**
	 * get the stop button
	 * @return ButtonME
	 */
	public ButtonME getButtonStop();

	/**
	 * set the stop button
	 * @param btnStop
	 */
	public void setButtonStop(ButtonME btnStop);

	/**
	 * get the beeper
	 * @return Beeper
	 */
    public Beeper getBeeper();

    /**
     * set the beeper
     * @param beeper
     */
	public void setBeeper(Beeper beeper);

	/**
	 * get the display
	 * @return Display
	 */
	public Display getDisplay();

	/**
	 * set the display
	 * @param display
	 */
	public void setDisplay(Display display);
	
	/**
	 * get the slider
	 * @return Slider
	 */
	public Slider getSlider();
	
	/**
	 * set the slider
	 * @param slider
	 */
	public void setSlider(Slider slider);
	
	/**
	 * set the timer
	 * @param timer
	 */
	public void setSchedule(Schedule timer);
	
	/**
	 * get the timer
	 * @return timer
	 */
	public Schedule getSchedule();
	
}
