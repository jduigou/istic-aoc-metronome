package command;

/**
 * Created by david on 06/10/2014.
 */
public interface Command
{

    public void execute();

}
