package javaFX;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Slider;

public class SliderFX implements view.Slider {
	
	private Slider slider;
	private OnSliderChangedCommand cmd;
	
	public SliderFX(Slider slider) {
		this.slider = slider;
	}

	@Override
	public void setSliderChangedCommand(OnSliderChangedCommand cmd) {
		this.cmd = cmd;
		
		slider.setOnMouseReleased(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				cmd.execute(slider.getValue());
			}
		});
	}	
}
