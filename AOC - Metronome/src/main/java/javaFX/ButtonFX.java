package javaFX;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import command.Command;
import view.ButtonME;

public class ButtonFX implements ButtonME {

	private Button button;

	private Command clickCommand;
	
	public ButtonFX(Button _button) {
		
		button = _button;
	
	}
	
	@Override
	public void setClickCommand(Command clickCommand) {
		
		this.clickCommand = clickCommand;
		
		button.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				clickCommand.execute();
			}
			
		});
	}
	
}
