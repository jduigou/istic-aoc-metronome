package javaFX;

import com.google.common.collect.Maps;
import command.Command;
import engine.Schedule;
import org.reactfx.util.FxTimer;
import org.reactfx.util.Timer;

import java.time.Duration;
import java.util.HashMap;

/**
 * Created by david on 15/10/2014.
 */
public class ScheduleFX implements Schedule {

    private HashMap<Command, Timer> timers;

    public ScheduleFX() {

        timers = Maps.newHashMap();

    }

    @Override
    public void runPeriodically(Command _cmd, int _period) {

        Timer timer = FxTimer.runPeriodically(
                Duration.ofMillis(_period),
                () -> _cmd.execute()
        );
        timers.put(_cmd, timer);

    }

    @Override
    public void runLater(Command _cmd, int _delay) {

        Timer timer = FxTimer.runLater(
                Duration.ofMillis(_delay),
                () -> _cmd.execute()
        );
        timers.put(_cmd, timer);

    }

    @Override
    public void cancel(Command _cmd) {

    	Timer t = timers.get(_cmd);
    	if (t != null) {
    		t.stop();
            timers.remove(_cmd);
    	}

    }
}
