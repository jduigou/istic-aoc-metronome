package javaFX;


import java.net.URISyntaxException;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import view.Beeper;

public class FXBeeper implements Beeper{

	private String soundURL = "sound.mp3";
	private Media sound;
	private MediaPlayer mediaPlayer;
	
	public FXBeeper() {
		try {
			sound = new Media(getClass().getResource(soundURL).toURI().toString());
			mediaPlayer = new MediaPlayer(sound);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void beep() {
		
		mediaPlayer.stop();
		mediaPlayer.play();
	}
	

}
