package javaFX;

import javafx.scene.control.Label;
import view.Display;

public class FXDisplay implements Display {

	private Label displayBeat;
	private Label displayBar;
	
	public FXDisplay(Label _displayBeat, Label _displayBar) {
		
		displayBeat = _displayBeat;
		displayBar = _displayBar;
		
	}

	@Override
	public void displayTempo(int _tempo) {
		
		displayBeat.setText(_tempo+"");
		
	}

	@Override
	public void displayBeatsPerBar(int _beatsPerBar) {
		
		displayBar.setText(_beatsPerBar+"");
		
	}

}
