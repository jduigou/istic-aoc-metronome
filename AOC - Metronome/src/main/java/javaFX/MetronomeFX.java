package javaFX;/**
 * Created by Johanna on 13/10/2014.
 */

import adapter.ViewAdapter;
import material.ViewMaterialImpl;
import controller.Controller;
import controller.ControllerFX;
import engine.MetronomeEngine;
import engine.MetronomeEngineImpl;
import engine.Schedule;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MetronomeFX extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {	
    	
    	// Create a new timer instance
    	Schedule timer = new ScheduleFX();
    	
    	// Create a new MetronomeEngine instance
    	MetronomeEngine me = new MetronomeEngineImpl();
    	
    	// Pass the new timer to the MetronomeEngine
    	me.setTimer(timer);
    	
    	// Create a new controller
    	Controller ctl = new Controller(me);  	    	
    	
    	// Load the FXML file
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("MetronomeFX.fxml"));
    	Parent root = loader.load();
    	
    	// Display the view on the screen
        primaryStage.setTitle("Metronome");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        
        // Init the JavaFX view Controller
        ViewMaterialImpl viewController = loader.getController();
        viewController.init(timer);
        
        // Link the Metronome's controller to the view controller
        ctl.setView(new ViewAdapter(viewController));
        
    }

}

