package javaFX;

import command.Command;
import engine.Schedule;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;
import view.LED;

public class FXLED implements LED {

	private Sphere sphere;
	private Schedule timer;
	
	/**
	 * Flash duration (in milliseconds)
	 */
	static private final int DURATION = 100;
	
	public FXLED(Sphere _sphere, Schedule _timer) {
		sphere = _sphere;
		timer = _timer;
	}
	public void flash() {
		
		//Creating PhongMaterial
    	PhongMaterial material = new PhongMaterial();
    	//Diffuse Color
    	material.setDiffuseColor(Color.ORANGE);
    	//Specular Color
    	material.setSpecularColor(Color.ORANGE);

    	sphere.setMaterial(material);
    	
    	timer.runLater(new Command() {
			@Override
			public void execute() {
				reset();
			}
		}, DURATION);

	}
	
	public void reset() {
		
		//Creating PhongMaterial
    	PhongMaterial material = new PhongMaterial();
    	//Diffuse Color
    	material.setDiffuseColor(Color.GRAY);
    	//Specular Color
    	material.setSpecularColor(Color.GRAY);

    	sphere.setMaterial(material);
		
	}
	
}
